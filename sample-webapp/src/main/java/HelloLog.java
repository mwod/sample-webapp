import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class HelloLog
 */
@WebServlet(name = "helloLog", urlPatterns = { "/hellolog" })
public class HelloLog extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(HelloLog.class);

	/**
	 * Default constructor.
	 */
	public HelloLog() {
		if (log.isInfoEnabled())
			log.info("HELLO, INFO!");
		if (log.isDebugEnabled())
			log.debug("HELLO, DEBUG!");

		log.error("HELLO, ERROR!");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html>");
		pw.println("<head><title>Hello, World</title></title>");
		pw.println("<body>");
		pw.println("<h1>Hello, Log!</h1>");
		pw.println("<p>");
		pw.println("In the standard output of your server should be a few log messages!");
		pw.println("</p>");
		pw.println("</body></html>");
	}

}
